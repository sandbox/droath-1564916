<?php

/**
 * @file
 * The field widget storage implementation.
 */
class field_widget_storage {

  // Entity properties.
  private $entity;
  private $bundle;
  private $entity_id;
  private $entity_type;
  private $revision_id;

  // Instances properties.
  private $instance;
  private $field_name;
  private $instance_id;

  // Storage properties.
  private $storage;

  function __construct($storage, $entity_type, $entity, $instance) {

    // Set the entity properties.
    if (!is_null($entity) && is_object($entity)) {

      // Extract the entity ids.
      list($entity_id, $revision_id, $bundle) = entity_extract_ids($entity_type, $entity);

      $this->entity      = $entity;
      $this->bundle      = $bundle;
      $this->entity_type = $entity_type;
      $this->entity_id   = $entity_id;
      $this->revision_id = $revision_id;
    }

    // Set the instances properties.
    if (!is_null($instance) && is_array($instance)) {
      $this->instance    = $instance;
      $this->field_name  = $instance['field_name'];
      $this->instance_id = $instance['id'];
    }

    // Set the storage properties.
    $this->storage = $storage;
  }

  /**
   * Define the default fields used in the widget storage schema.
   *
   * @return An array of schema default fields.
   */
  public function storage_schema_default_fields() {
    return array(
      'entity_id',
      'revision_id',
      'entity_type',
      'bundle',
      'language',
      'field_name',
      'instance_id',
      'delta',
    );
  }

  /**
   * Prepare the records to be saved or updated by the widget storage schema.
   */
  public function save() {
    $storage = $this->get_storage();
    if (is_null($storage)) {
      return;
    }
    $schema = $storage['schema'];

    // Clean up orphan records as a file might have been removed.
    $this->storage_remove_orphan_records_if_exist($schema['name']);

    if (empty($storage['fields'])) {
      return;
    }

    // Initialize the storage records array.
    $records = array();
    $records['entity_id']   = $this->get_entity_id();
    $records['revision_id'] = $this->get_revision_id();
    $records['entity_type'] = $this->get_entity_type();
    $records['instance_id'] = $this->get_instance_id();
    $records['bundle']      = $this->get_bundle();

    // Iterate through the storage fields and build the records array.
    foreach ($storage['fields'] as $field_name => $field_info) {
      foreach ($field_info as $language => $elements) {
        foreach ($elements as $delta => $fields) {

          // Record the field information
          $records['language']    = $language;
          $records['field_name']  = $field_name;
          $records['delta']       = $delta;

          // Record the field values that are defined by the widget storage schema.
          foreach (array_intersect_key($fields, $schema['fields']) as $key => $value) {
            $records[$key] = $value;
          }

          // Pass the records that need to be saved or updated for that field.
          $this->write($records, $schema);
        }
      }
    }
  }

  /**
   * Delete the widgets storage data by entity id.
   */
  public function delete_by_entity_id() {
    $storage = $this->get_storage();
    if (!$storage['schema']) {
      return;
    }
    $schema_table = $storage['schema']['name'];

    $query = db_delete($schema_table)
      ->condition('entity_id', $this->get_entity_id())
      ->execute();
  }

  /**
   * Delete the widgets storage data by instance id.
   */
  public function delete_by_instance_id() {
    $storage = $this->get_storage();
    if (!$storage['schema']) {
      return;
    }
    $schema_table = $storage['schema']['name'];

    $query = db_delete($schema_table)
      ->condition('instance_id', $this->get_instance_id())
      ->execute();
  }

  /**
   * Load the widget storage field values.
   *
   * @return An array of loaded field values.
   */
  public function load_by_entity_id() {
    $storage = $this->get_storage();
    if (!$storage['schema']) {
      return;
    }
    $schema = $storage['schema'];

    $query = db_select($schema['name'], 'st')
      ->fields('st', array())
      ->condition('entity_id', $this->get_entity_id())
      ->execute();
    $load = array();

    // Instance for this entity.
    $load['instance'] = $this->get_instance();

    // Storage values for this entity id.
    $load['storage_values'] = $query->fetchAllAssoc('delta', PDO::FETCH_ASSOC);

    // Storage fields that its responsible for.
    $load['storage_fields'] = array_diff(array_keys($schema['fields']), $this->storage_schema_default_fields());

    return $load;
  }

  /**
   * Writes records based on the widget storage schema for both saving and updating.
   */
  private function write($records, $schema) {
    if (!isset($schema['name'])) {
      return;
    }
    $schema_table = $schema['name'];

    // Primary keys for the storage schema.
    $primary_key = ($this->entity->is_new) ? array() : $schema['primary key'];

    // If we have primary keys, then we must be updating the widget storage schema table
    // but that doesn't mean we aren't saving a new field cardinality. So we need
    // to check if field data exists or not, before writing the field data.
    if (!empty($primary_key)) {
      if (!$this->field_data_exists($schema_table, $records)) {
        $primary_key = array();
      }
    }

    // Write the new or updated field values to the widget storage schema table.
    // TODO: We might want to look into using db_merge() instead.
    drupal_write_record($schema_table, $records, $primary_key);
  }

  /**
   * Determine if a field exist based on the entity_id, field_name, and delta conditions.
   *
   * @param $schema_table
   *  - A schema table name.
   * @param $records
   *  - A array containing the field and values.
   *
   * @return A boolean value based on if the field exist or not.
   */
  private function field_data_exists($schema_table, $records) {
    if (!isset($schema_table) && !isset($records)) {
      return;
    }
    $query = db_select($schema_table)
      ->condition('entity_id', $records['entity_id'])
      ->condition('field_name', $records['field_name'])
      ->condition('delta', $records['delta'])
      ->countQuery()
      ->execute();

    return (bool) $query->fetchField();
  }

  /**
   * Determine if a file exist for the entity.
   *
   * @param $fid
   *  - A file id.
   *
   * @return A boolean value based on if the file exist or not.
   */
  private function entity_file_exist($fid) {
    if (!isset($fid)) {
      return;
    }
    $query = db_select('file_usage', 'fu')
      ->condition('fid', $fid)
      ->condition('id', $this->get_entity_id())
      ->countQuery()
      ->execute();
    return (bool) $query->fetchField();
  }

  /**
   * Remove the storage orphan records.
   *
   * @param $schema_table
   *  - A storage schema table name.
   */
  private function storage_remove_orphan_records_if_exist($schema_table) {
    if (!isset($schema_table)) {
      return;
    }
    $records_deleted = FALSE;
    $entity_id = $this->get_entity_id();

    // Get the storage records by entity id.
    $query = db_select($schema_table, 'st')
      ->fields('st', array('delta', 'fid'))
      ->condition('entity_id', $entity_id)
      ->orderBy('delta', 'ASC')
      ->execute();
    $records = $query->fetchAll(PDO::FETCH_ASSOC);

    // Bail out if we have no records returned.
    if (empty($records)) {
      return;
    }

    // Iterate through the records to see if the file still exist.
    foreach ($records as $key => $record) {
      $fid   = $record['fid'];
      $delta = $record['delta'];

      // See if the file exist for this entity.
      if ($this->entity_file_exist($fid)) {
        continue;
      }

      // If the file doesn't exist then remove that record.
      $query = db_delete($schema_table)
        ->condition('entity_id', $entity_id)
        ->condition('delta', $delta)
        ->condition('fid', $fid)
        ->execute();

      unset($records[$key]);

      $record_deleted = TRUE;
    }

    // If we deleted a record we need to re-index the existing records.
    if (isset($record_deleted)) {
      foreach (array_values($records) as $index => $record) {
        $query = db_update($schema_table)
          ->fields(array('delta' => $index))
          ->condition('entity_id', $entity_id)
          ->condition('delta', $record['delta'])
          ->execute();
      }
    }
  }

  /**
   * Get the current object entity id.
   */
  private function get_entity_id() {
    $entity_id = $this->entity_id;
    return (isset($entity_id)) ? $entity_id : NULL;
  }

  /**
  * Get the current object revision id.
  */
  private function get_revision_id() {
    $revision_id = $this->revision_id;
    return (isset($revision_id)) ? $revision_id : NULL;
  }

  /**
   * Get the current object entity type.
   */
  private function get_entity_type() {
    $entity_type = $this->entity_type;
    return (isset($entity_type)) ? $entity_type : NULL;
  }

  /**
  * Get the current object bundle.
  */
  private function get_bundle() {
    $bundle = $this->bundle;
    return (isset($bundle)) ? $bundle : NULL;
  }

  /**
  * Get the current object storage.
  */
  private function get_storage() {
    $storage = $this->storage;
    return (isset($storage)) ? $storage : NULL;
  }

  /**
  * Get the current object instance.
  */
  private function get_instance() {
    $instance = $this->instance;
    return (isset($instance)) ? $instance : NULL;
  }

  /**
  * Get the current object instance id.
  */
  private function get_instance_id() {
    $instance_id = $this->instance_id;
    return (isset($instance_id)) ? $instance_id : NULL;
  }

  /**
  * Get the current object field name.
  */
  private function get_field_name() {
    $field_name = $this->field_name;
    return (isset($field_name)) ? $field_name : NULL;
  }
}
